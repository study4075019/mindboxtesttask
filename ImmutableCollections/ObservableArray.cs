using IndexerObserver;

namespace ImmutableCollections;

public class ObservableArray<TValue> : 
    IFixedLengthArray<TValue>, 
    INotifyIndexerValueChanging<TValue>
{
    private readonly ReaderWriterLockSlim _rwLock = new ReaderWriterLockSlim();

    private readonly TValue[] _values;

    public ObservableArray(int length) 
    {
        _values = new TValue[length];
    }

    public ObservableArray(params TValue[] paramsValues) : this(length: paramsValues.Length)
    {
        for (int i = 0; i < Length; ++i)
        {
            this[i] = paramsValues[i];
        }
    }

    public ObservableArray(IEnumerable<TValue> enumerableValues) : this(length: enumerableValues.Count())
    {
        int i = 0;
        foreach (var value in enumerableValues)
        {
            this[i] = value;
            ++i;
        }
    }

    public event IndexerValueChangingEventHandler<TValue> IndexerValueChanging;

    private void OnArrayValueChanging(TValue currentValue, int index, TValue newValue) 
    => IndexerValueChanging?.Invoke(
        this, 
        new IndexerValueChangingEventArgs<TValue>(
            currentValue, 
            index, 
            newValue
        )
    );

    public TValue this[int index] 
    { 
        get 
        {
            if (index < 0 || index >= Length)
                throw new ArgumentOutOfRangeException("index");

            _rwLock.EnterReadLock();
            try { return _values[index]; }
            finally { _rwLock.ExitReadLock(); }
        }
        set
        {
            if (index < 0 || index >= Length)
                throw new ArgumentOutOfRangeException("index");

            OnArrayValueChanging(_values[index], index, value);
            
            _rwLock.EnterWriteLock();
            try { _values[index] = value; }
            finally { _rwLock.ExitWriteLock(); }
        }
    }

    public int Length {
        get {
            _rwLock.EnterReadLock();
            try { return _values.Length; }
            finally { _rwLock.ExitReadLock(); }
        }
    }

    public IEnumerator<TValue> GetEnumerator() => _values.Cast<TValue>().GetEnumerator();

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() => _values.GetEnumerator();
}