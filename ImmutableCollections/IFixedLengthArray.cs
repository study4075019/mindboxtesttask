namespace ImmutableCollections;

public interface IFixedLengthArray<TValue> : IEnumerable<TValue>
{
    TValue this[int index] { get; set; }

    int Length { get; }
}