namespace ShapesLib.Shapes;

public class Circle : IHaveArea
{
    private double _radius;

    /// <summary>
    /// Радиус круга
    /// </summary>
    /// <remarks>
    /// Решил оставить пользователю возможность сделать из круга точку с радиусом равным 0.
    /// </remarks>
    public double Radius 
    { 
        get => _radius; 
        set
        { 
            if (value < 0)
                throw new ArgumentException("Circle radius can't be negative", nameof(value));
            _radius = value; 
        }
    }

    public double Area => Math.PI * Radius * Radius;
}