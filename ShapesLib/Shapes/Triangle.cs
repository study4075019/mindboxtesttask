using IndexerObserver;
using ImmutableCollections;

namespace ShapesLib.Shapes;

/// <summary>
/// Треугольник
/// </summary>
/// <remarks>
/// Прошу обратить внимание на то, что в данном классе реализован метод IsRightTriangle
/// </remarks>
public class Triangle : IHaveArea
{
    public Triangle(double side0, double side1, double side2)
    {
        _sides = new ObservableArray<double>(length: 3);
        Sides[0] = side0;
        Sides[1] = side1;
        _sides.IndexerValueChanging += SideValidation;
        Sides[2] = side2;
    }

    private static readonly IList<int> _indexes = new List<int>(3) { 0, 1, 2 };
    private (int, int, int) GetMaxOthetOtherIndexes(IList<double> checkList)
    {
        var maxIndex = checkList.IndexOf(checkList.Max());
        var otherIndexes = _indexes.Where(i => i != maxIndex).ToArray();
        return (maxIndex, otherIndexes[0], otherIndexes[1]);
    }

    private void SideValidation(object? sender, IndexerValueChangingEventArgs<double> e)
    {
        if (e.NewValue <= 0)
            throw new InvalidOperationException(
                "The side of a triangle can't be non-positive: " + 
                $"{e.NewValue} at {nameof(Sides)} index {e.Index} " +
                $"while trying to change value {e.CurrentValue} to value {e.NewValue} at index {e.Index}"
            );

        var checkList = new List<double>(Sides);
        checkList[e.Index] = e.NewValue;

        (int maxIndex, int otherIndex1, int otherIndex2) = GetMaxOthetOtherIndexes(checkList);

        if (checkList[otherIndex1] + checkList[otherIndex2] <= checkList[maxIndex])
            throw new InvalidOperationException(
                "The sum of two sides of a triangle must be greater than the value " + 
                $"of the third side: value {checkList[maxIndex]} at index {maxIndex} " + 
                $"while trying to change value {e.CurrentValue} to value {e.NewValue} at index {e.Index}"
            );
    }   

    private readonly ObservableArray<double> _sides;
    public IFixedLengthArray<double> Sides => _sides;

    public double Perimeter => Sides[0] + Sides[1] + Sides[2];
    public double HalfPerimeter => Perimeter / 2;
    public double Area {
        get {
            var p = HalfPerimeter;
            return Math.Sqrt(p * (p - Sides[0]) * (p - Sides[1]) * (p - Sides[2]));
        }
    }

    /// <summary>
    /// Метод определяет, является ли треугольник прямоугольным
    /// </summary>
    /// <param name="epsilon">точность</param>
    /// <returns>true если прямоугольный, иначе - false</returns>
    public bool IsRightTriangle(double epsilon = 0)
    {
        var checkList = new List<double>(Sides);
        checkList.Sort();
        return 
            Math.Abs(
                Math.Sqrt(
                    Math.Pow(checkList[0], 2) + 
                    Math.Pow(checkList[1], 2)
                ) - checkList[2]
            ) <= epsilon;
    }
}