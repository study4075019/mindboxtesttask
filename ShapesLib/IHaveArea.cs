﻿namespace ShapesLib;

/// <summary>
/// Данный интерфейс позволит легко добавлять новые фигуры 
/// <para/>(достаточно реализовать всего одно свойство), 
/// <para/>А так же вычислять площадь, не зная конкретный тип фигуры
/// </summary>
/// <remarks>
/// Например, так
/// <code> 
/// var shapes = new IHaveArea[]
/// {
///     new Circle { Radius = Math.Sqrt(1 / Math.PI) },
///     new Triangle(3, 4, 5)
/// };
/// foreach (var shape in shapes)
/// {
///     Console.WriteLine(shape.Area);
/// }
/// </code>
/// В результате будет выведено:
/// <para/>1
/// <para/>6
/// </remarks>
public interface IHaveArea
{
    double Area { get; }
}
