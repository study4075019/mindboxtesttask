namespace IndexerObserver;

public interface INotifyIndexerValueChanging<TValue>
{
    event IndexerValueChangingEventHandler<TValue> IndexerValueChanging;
}