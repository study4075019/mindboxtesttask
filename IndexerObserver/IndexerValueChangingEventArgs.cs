namespace IndexerObserver;

public class IndexerValueChangingEventArgs<T> : EventArgs
{
    public readonly T? CurrentValue;
    public readonly int Index;
    public readonly T? NewValue;

    public IndexerValueChangingEventArgs(T? oldValue, int index, T? newValue) 
        => (CurrentValue, Index, NewValue) = (oldValue, index, newValue);
}