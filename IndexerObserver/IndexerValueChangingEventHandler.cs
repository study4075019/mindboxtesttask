namespace IndexerObserver;

public delegate void IndexerValueChangingEventHandler<T>(object? sender, IndexerValueChangingEventArgs<T> e);
