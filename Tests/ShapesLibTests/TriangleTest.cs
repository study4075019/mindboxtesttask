namespace ShapesLibTests;

[TestClass]
public class TriangleTest
{
    [TestMethod]
    public void TestArea()
    {
        var triangle = new Triangle(3, 4, 5);
        Assert.AreEqual(6, triangle.Area);
    }

    [TestMethod]
    public void TestSides()
    {
        Assert.ThrowsException<InvalidOperationException>(
            () => {
                var triangle = new Triangle(-3, 4, 5);
            }
        );

        Assert.ThrowsException<InvalidOperationException>(
            () => {
                var triangle = new Triangle(3, -4, 5);
            }
        );

        Assert.ThrowsException<InvalidOperationException>(
            () => {
                var triangle = new Triangle(3, 4, -5);
            }
        );
    }

    [TestMethod]
    public void TestPerimeter()
    {
        var triangle = new Triangle(3, 4, 5);
        Assert.AreEqual(12, triangle.Perimeter);
    }

    [TestMethod]
    public void TestHalfPerimeter()
    {
        var triangle = new Triangle(3, 4, 5);
        Assert.AreEqual(6, triangle.HalfPerimeter);
    }

    [TestMethod]
    public void TestIsRightTriangle()
    {
        var triangle = new Triangle(3, 4, 5);
        Assert.IsTrue(triangle.IsRightTriangle());
        triangle.Sides[0] = 4;
        Assert.IsFalse(triangle.IsRightTriangle());
    }
}