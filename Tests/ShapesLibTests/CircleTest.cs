namespace ShapesLibTests;

[TestClass]
public class CircleTest
{
    [TestMethod]
    public void TestRadius() => Assert.ThrowsException<ArgumentException>(() => new Circle().Radius = -0.001);

    [TestMethod]
    public void TestArea()
    {
        Circle circle = new Circle()
        {
            Radius = 2.5
        };
        Assert.AreEqual(Math.Pow(2.5, 2) * Math.PI, circle.Area);
    }
}