using ImmutableCollections;
using IndexerObserver;

namespace ImmutableCollectionsTests;

[TestClass]
public class ObservableArrayTest
{
    [TestMethod]
    public void TestGetEnumerator()
    {
        Assert.IsTrue(new ObservableArray<object>().GetEnumerator() is IEnumerator<object>);
    }

    [TestMethod]
    public void TestLength()
    {
        var arr = new ObservableArray<object>(3);
        Assert.AreEqual(3, arr.Length);

        arr = new ObservableArray<object>(1, 2, 3);
        Assert.AreEqual(3, arr.Length);

        arr = new ObservableArray<object>(paramsValues: 5);
        Assert.AreEqual(1, arr.Length);
    }

    [TestMethod]
    public void TestIndexer()
    {
        var arr = new ObservableArray<int>(3);
        for (int i = 0; i < arr.Length; ++i)
        {
            arr[i] = i;
        }
        for (int i = 0; i < 3; ++i)
        {
            Assert.AreEqual(i, arr[i]);
        }
    }

    [TestMethod]
    public void TestEvent()
    {
        var arr = new ObservableArray<double>(3);

        IndexerValueChangingEventArgs<double>? eArg = null;
        bool isEventHandled = false;

        arr.IndexerValueChanging += 
            (sender, e) => 
            { 
                eArg = e; 
                isEventHandled = true; 
            };

        arr[1] = 5.5;

        Assert.IsTrue(isEventHandled);
        Assert.AreNotEqual(null, eArg);
        Assert.AreEqual(0, eArg?.CurrentValue);
        Assert.AreEqual(1, eArg?.Index);
        Assert.AreEqual(5.5, eArg?.NewValue);
    }
}